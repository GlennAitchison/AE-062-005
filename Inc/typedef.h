/**
  ******************************************************************************
  * File Name         : typedef.h
  * Description       : This file provides hungarian notation in liine with coding standard AE-000-001-01 (28/05/2019)
  *                   
  * Revision		      :	1.0
  * Date			        : 28/05/2019
	*			
  * Author			      : G.Aitchison
  * Toolchain		      : MDK-ARM Cortex-M0/M0+256 for ST Version 5.23
  * Compiler		      : Armcc.exe V5.06 update 4 (build 422)
  * IDE 			        : uVision V5
  * Target Processor	: STM32L01XXXXX
  *
  ******************************************************************************
  *
  * COPYRIGHT(c) 2021 Altilium Engineering Ltd, All rights reserved
  * Tel 07494684737
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Embedded Electronics Ltd nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  **/
  
  #ifndef TYPEDEF_H
#define TYPEDEF_H
#endif /* TYPEDEF_H */  

/** Includes ****************************************************************/
#include "stdint.h"

/** # Defines ****************************************************************/

/** External References ******************************************************/

/** Global Variables *********************************************************/

/** Function Prototypes*******************************************************/

/** Functions ****************************************************************/


typedef	signed char		  S8;
typedef	signed short		S16;
typedef	signed int		  S32;
typedef	signed long 		S64;
typedef	unsigned char  	U8;
typedef	unsigned short 	U16;
typedef	unsigned int	 	U32;
typedef	unsigned long		U64;
typedef	float	 		      F32;
typedef	double	 		    F64;
typedef	long double  		F128;


/**************** End of File - typedef.h *************/
