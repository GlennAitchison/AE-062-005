/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define CH1_Gate_Pin GPIO_PIN_3
#define CH1_Gate_GPIO_Port GPIOA
#define CH2_Gate_Pin GPIO_PIN_4
#define CH2_Gate_GPIO_Port GPIOA
#define CH3_Gate_Pin GPIO_PIN_6
#define CH3_Gate_GPIO_Port GPIOA
#define CH4_Gate_Pin GPIO_PIN_7
#define CH4_Gate_GPIO_Port GPIOA
#define Input_4_Pin GPIO_PIN_0
#define Input_4_GPIO_Port GPIOB
#define Input_3_Pin GPIO_PIN_1
#define Input_3_GPIO_Port GPIOB
#define EXT_INT1_Pin GPIO_PIN_8
#define EXT_INT1_GPIO_Port GPIOA
#define EXT_INT1_EXTI_IRQn EXTI4_15_IRQn
#define Input_1_Pin GPIO_PIN_11
#define Input_1_GPIO_Port GPIOA
#define Input_2_Pin GPIO_PIN_12
#define Input_2_GPIO_Port GPIOA
#define HL_Pin GPIO_PIN_15
#define HL_GPIO_Port GPIOA
#define Output_2_Pin GPIO_PIN_3
#define Output_2_GPIO_Port GPIOB
#define Output_3_Pin GPIO_PIN_4
#define Output_3_GPIO_Port GPIOB
#define Output_4_Pin GPIO_PIN_5
#define Output_4_GPIO_Port GPIOB
#define Output_5_Pin GPIO_PIN_6
#define Output_5_GPIO_Port GPIOB
#define Output_6_Pin GPIO_PIN_7
#define Output_6_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

#define TRUE	1
#define FALSE	0

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
