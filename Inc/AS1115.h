/**
  ******************************************************************************
  * File Name         : AS1115.h
  * Description       : This file builds on the I2C driver for communication with
	*											the AS1115 64 bit LED matrix driver
  * Revision		      :	1.0
  * Date			        : 21/08/2021
	*			
  * Author			      : G.Aitchison
  * Toolchain		      : MDK-ARM Cortex-M0/M0+256 for ST Version 5.35.02
  * Compiler		      : Armcc.exe V5.06 update 7 (build 960)
  * IDE 			        : uVision V5
  * Target Processor	: STM32L01XXXXX
  *
  ******************************************************************************
  *
  * COPYRIGHT(c) 2021 Altilium Engineering Ltd, All rights reserved
  * Tel 07494684737
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Embedded Electronics Ltd nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  **/
  
  #ifndef AS1115_H
#define AS1115_H
#endif /* AS1115_H */  

/** Includes ****************************************************************/
#include "typedef.h"

/** # Defines ****************************************************************/
typedef enum /*Intensity values in the AS1115 */
{
	SHUTDOWN_RESET = 0,
	SHUTDOWN_NO_RESET = 0x80,
	NORMAL_OPERATION_RESET = 0x01,
	NORMAL_OPERATION_NO_RESET = 0x81,
	
}AS1115_Shutdown_Codes;

/** External References ******************************************************/

/** Global Variables *********************************************************/

/** Function Prototypes*******************************************************/
void vTest_I2C(void);
void vIntensity(U8 u8Intensity);
void vDecode_Mode(U8 u8Mode);
void vShutdown(U8 u8Shutdown);
void vDisplay_Test_Mode(U8 u8Test_mode);
void vWrite_Digit(U8 u8Digit, U8 u8Value);
void vFeature(U8 u8Feature);
void vAS1115_Init(void);

void vAS1115_Poll(void);

void vData_Read(void);
/** Functions ****************************************************************/




/**************** End of File - AS1115.h *************/

