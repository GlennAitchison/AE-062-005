/**
  ******************************************************************************
  * File Name         : AS1115.c
  * Description       : This file builds on the I2C driver for communication with
	*											the AS1115 64 bit LED matrix driver
  * Revision		      :	1.0
  * Date			        : 21/08/2021
	*			
  * Author			      : G.Aitchison
  * Toolchain		      : MDK-ARM Cortex-M0/M0+256 for ST Version 5.35.02
  * Compiler		      : Armcc.exe V5.06 update 7 (build 960)
  * IDE 			        : uVision V5
  * Target Processor	: STM32L01XXXXX
  *
  ******************************************************************************
  *
  * COPYRIGHT(c) 2021 Altilium Engineering Ltd, All rights reserved
  * Tel 07494684737
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of Embedded Electronics Ltd nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  **/


/** Includes ****************************************************************/
#include "typedef.h"
#include "AS1115.h"
#include "i2c.h"

/** # Defines ****************************************************************/

typedef enum /*Registers in the AS1115 */
{
	AS1115_Digit_0 = 1,
	AS1115_Digit_1,
	AS1115_Digit_2,
	AS1115_Digit_3,
	AS1115_Digit_4,
	AS1115_Digit_5,
	AS1115_Digit_6,
	AS1115_Digit_7,
	AS1115_Decode_Mode,
	AS1115_Global_Intensity,
	AS1115_Scan_Limit,
	AS1115_Shutdown,
	AS1115_Self_Addressing,
	AS1115_Feature,
	AS1115_Display_Test_Mode,
	AS1115_DIG0_DIG1_Intensity,
	AS1115_DIG2_DIG3_Intensity,
	AS1115_DIG4_DIG5_Intensity,
	AS1115_DIG6_DIG7_Intensity,
	AS1115_Diagnostic_DIG0,
	AS1115_Diagnostic_DIG1,
	AS1115_Diagnostic_DIG2,
	AS1115_Diagnostic_DIG3,
	AS1115_Diagnostic_DIG4,
	AS1115_Diagnostic_DIG5,
	AS1115_Diagnostic_DIG6,
	AS1115_Diagnostic_DIG7,
	AS1115_KEYA,
	AS1115_KEYB,
	
	AS115_MODE_READ_FIRST,
	AS115_MODE_NORMAL,
	AS115_MODE_INIT_FEATURE,
	AS115_MODE_INIT_DISPLAY,
	AS115_MODE_INIT_IND_INTENSITY,
	AS115_MODE_READ,
	AS115_MODE_UPDATE,
}AS1115;




/** External References ******************************************************/
extern I2C_HandleTypeDef hi2c1;
U8 u8i2c_data[6];
U8 u8Count = 1;
U8 u8i2c_data_read[32];

U8 u8Poll_Mode = AS115_MODE_READ_FIRST;

/** Global Variables *********************************************************/

/** Function Prototypes*******************************************************/

void vTransmit(U8 u8MessageLength);
	
/** Functions ****************************************************************/
void vTest_I2C(void)
{
	u8i2c_data[0] = u8Count;
	u8i2c_data[1] = 0x03;
	HAL_I2C_Master_Transmit(&hi2c1,0,u8i2c_data,2,100);
	u8Count++;
	if(9 == u8Count)
	{
		u8Count = 1;
	}
 	//HAL_I2C_Master_Receive_IT(&hi2c1,0x00,u8i2c_data,2);
	
}

void vIntensity(U8 u8Intensity)
{
	u8i2c_data[0] = AS1115_Global_Intensity;
	u8i2c_data[1] = u8Intensity;
	vTransmit(2);
	
}


void vFeature(U8 u8Feature)
{
	u8i2c_data[0] = AS1115_Feature;
	u8i2c_data[1] = u8Feature;
	vTransmit(2);
	
}

void vWrite_Digit(U8 u8Digit, U8 u8Value)
{
	u8i2c_data[0] = u8Digit;
	u8i2c_data[1] = u8Value;
	vTransmit(2);
}

void vDecode_Mode(U8 u8Mode)
{
		u8i2c_data[0] = AS1115_Decode_Mode;
		u8i2c_data[1] = u8Mode;
		vTransmit(2);
}

void vTransmit(U8 u8MessageLength)
{
	HAL_I2C_Master_Transmit(&hi2c1,0,u8i2c_data,u8MessageLength,100);
}

void vShutdown(U8 u8Shutdown)
{
		u8i2c_data[0] = AS1115_Shutdown;
		u8i2c_data[1] = u8Shutdown;
		vTransmit(2);
}

void vData_Read(void)
{
	u8i2c_data[0] = AS1115_Digit_0;
	HAL_I2C_Master_Transmit(&hi2c1,0,u8i2c_data,1,100);
	
	HAL_I2C_Master_Receive(&hi2c1,0,u8i2c_data_read,30,100);
}


U8 u8Register = AS1115_Shutdown;
U8 u8Data = 0x81;

/*
	AS1115_Digit_0 = 1,
	AS1115_Digit_1,
	AS1115_Digit_2,
	AS1115_Digit_3,
	AS1115_Digit_4,
	AS1115_Digit_5,
	AS1115_Digit_6,
	AS1115_Digit_7,
	AS1115_Decode_Mode,
	AS1115_Global_Intensity,
	AS1115_Scan_Limit,
	AS1115_Shutdown,
	AS1115_Self_Addressing,
	AS1115_Feature,
	AS1115_Display_Test_Mode,
	AS1115_DIG0_DIG1_Intensity,
	AS1115_DIG2_DIG3_Intensity,
	AS1115_DIG4_DIG5_Intensity,
	AS1115_DIG6_DIG7_Intensity,
	AS1115_Diagnostic_DIG0,
	AS1115_Diagnostic_DIG1,
	AS1115_Diagnostic_DIG2,
	AS1115_Diagnostic_DIG3,
	AS1115_Diagnostic_DIG4,
	AS1115_Diagnostic_DIG5,
	AS1115_Diagnostic_DIG6,
	AS1115_Diagnostic_DIG7,
	AS1115_KEYA,
	AS1115_KEYB,
	*/


	/*AS115_MODE_READ_FIRST,
	AS115_MODE_NORMAL,
	AS115_MODE_INIT_FEATURE,
	AS115_MODE_INIT_DISPLAY,
	AS115_MODE_INIT_IND_INTENSITY,
	AS115_MODE_READ,
	AS115_MODE_UPDATE,
	
	*/
	
void vAS1115_Poll(void)
{
	switch(u8Poll_Mode)
	{
		
		/*Read all the dat from the chip, verifies connection */
		case AS115_MODE_READ_FIRST:
		{
			u8i2c_data[0] = 1;
			HAL_I2C_Master_Transmit(&hi2c1,0,u8i2c_data,1,100);
			HAL_I2C_Master_Receive(&hi2c1,0,u8i2c_data_read,29,100);			
			u8Poll_Mode = AS115_MODE_NORMAL;
			
		}break;
		
		/*Takes device out of shutdown */
		case AS115_MODE_NORMAL:
		{
			u8i2c_data[0] = AS1115_Shutdown;
			u8i2c_data[1] = 0x81;
			u8Poll_Mode = AS115_MODE_INIT_FEATURE;
			vTransmit(2);
			
		}break;
		
		/*Sets feature set Table 21 AS1115 DS Revision 1.08*/
		/*
		Internal Clock, Reset Disable, Selects B coding (irrelevant for this display), 
		Disables blink, leaves blink paramaeters as default. 
		*/
		case AS115_MODE_INIT_FEATURE:
		{
			u8i2c_data[0] = AS1115_Feature;
			u8i2c_data[1] = 0x00;
			u8Poll_Mode = AS115_MODE_INIT_DISPLAY;
			vTransmit(2);
		}break;
		
		case AS115_MODE_INIT_DISPLAY:
		{
			u8i2c_data[0] = AS1115_Decode_Mode;
			u8i2c_data[1] = 0x00;		/*Decode Register No decodeing on this as it is a matris display */
			u8i2c_data[2] = 0x0F;		/*Global intensity set to full intensity */	
			u8i2c_data[3] = 0x04;		/*Display DIG0 to DIG 4 - 5 x 7 matrix */
			u8Poll_Mode = AS115_MODE_READ;
			vTransmit(4);
		}break;
		
		/*Set individual intensity, not sure if this needs updating but belts an braces to statr */
		case AS115_MODE_INIT_IND_INTENSITY:
		{
			u8i2c_data[0] = AS1115_DIG0_DIG1_Intensity;
			u8i2c_data[1] = 0xFF;		
			u8i2c_data[2] = 0xFF;			
			u8i2c_data[3] = 0xFF;		
			u8i2c_data[4] = 0xFF;		
			u8Poll_Mode = AS115_MODE_READ;
			vTransmit(5);
			
		}break;
		
		case AS115_MODE_READ:
		{
			u8i2c_data[0] = 1;
			HAL_I2C_Master_Transmit(&hi2c1,0,u8i2c_data,1,100);
			HAL_I2C_Master_Receive(&hi2c1,0,u8i2c_data_read,30,100);
		
			u8Data = 0x00;
			u8Poll_Mode = AS115_MODE_UPDATE;
			
			
		}break;
		
		
		/*Wait mode for increment of data */
		case AS115_MODE_UPDATE:
		{
			u8Data = 0x01;
			u8i2c_data[0] = 0x01;
			u8i2c_data[1] = 0x02;		
			u8i2c_data[2] = 0x04;			
			u8i2c_data[3] = 0x08;		
			u8i2c_data[4] = 0x10;
			u8i2c_data[5] = 0x00;
			vTransmit(6);
			
		}break;
	}
}

/*Write stream dat for all registers to the AS1115 */
void vAS1115_Init(void)
{
	
	u8i2c_data[0] = AS1115_Digit_0;
	HAL_I2C_Master_Transmit(&hi2c1,0,u8i2c_data,1,100);
	
	
	u8i2c_data_read[0] =  0x01;//AS1115_Digit_0 = 1,
	u8i2c_data_read[1] =  0x02;//AS1115_Digit_1,
	u8i2c_data_read[2] =  0x03;//AS1115_Digit_2,
	u8i2c_data_read[3] =  0x04;//AS1115_Digit_3,
	u8i2c_data_read[4] =  0x05;//AS1115_Digit_4,
	u8i2c_data_read[5] =  0x06;//AS1115_Digit_5,
	u8i2c_data_read[6] =  0x07;//AS1115_Digit_6,
	u8i2c_data_read[7] =  0x08;//AS1115_Digit_7,
	
	u8i2c_data_read[8] =  0xFF;//AS1115_Decode_Mode,
	u8i2c_data_read[9] =  0x0F;//AS1115_Global_Intensity,
	u8i2c_data_read[10] = 0x07;//AS1115_Scan_Limit,
	u8i2c_data_read[11] = 0x81;//AS1115_Shutdown,
	u8i2c_data_read[12] = 0x00;//AS1115_Self_Addressing,
	u8i2c_data_read[13] = 0x10;//AS1115_Feature,
	u8i2c_data_read[14] = 0x00;//AS1115_Display_Test_Mode,
	
	u8i2c_data_read[15] = 0xFF;//AS1115_DIG0_DIG1_Intensity,
	u8i2c_data_read[16] = 0xFF;//AS1115_DIG2_DIG3_Intensity,
	u8i2c_data_read[17] = 0xFF;//AS1115_DIG4_DIG5_Intensity,
	u8i2c_data_read[18] = 0xFF;//AS1115_DIG6_DIG7_Intensity,
	

	HAL_I2C_Master_Transmit(&hi2c1,0,u8i2c_data_read,19,100);

}

void vDisplay_Test_Mode(U8 u8Test_mode)
{
		u8i2c_data[0] = AS1115_Display_Test_Mode;
		u8i2c_data[1] = 0x00;
		vTransmit(2);
}

/**************** End of File - AS1115.c *************/
